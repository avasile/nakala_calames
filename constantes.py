WORKFOLDER= "paquetTestApi"
EMAIL="TO REPLACE"
TOKEN="TO REPLACE"
HANDLEPROJECT = "11280/81146c51"

INPUTFORARCHDESC="data/inputForArchdesc.csv"
OUTPUTARCHDESC="data/headerArchdesc.xml"
INPUTFILE="data/csv_input.csv"
OUTPUCOPY="data/csv_input_copy.csv"
OUTPUTCALAMES ="data/sortieEADNiveauxC.xml"

CODEROLEAUTEUR = {"Auteur":"070", "Annotateur":"020",
                  "Auteur adapté": "100", "Auteur supposé": "330",
                  "Commentateur": "212", "Compilateur": "220", "Commanditaire": "commanditaire",
                   "Participant": "participant", "Producteur": "producteur", "Fabricant": "fabricant",
                  "Copiste":"700", "Dédicataire":"280", "Destinataire": "660",
                  "Editeur commercial":"650", "Editeur scientifique": "340",
                  "Illustrateur": "440", "Imprimeur ou éditeur": "610", "Interprète":"590",
                  "Propriétaire précédent": "390", "Relieur": "110", "Traducteur": "730"}

# CALAMES
ATTRIBIDENTIFIANTCALAMES = "_631139801_"
NIVEAUDEUX = "series"
NIVEAUTROIX = "item"


# NAKALA
SPARQLPOINT = "https://www.nakala.fr/sparql/"

# requête sparq pour récupérer toutes les données de la collection Archives de campament, grâce au handle de la collection: 11280/c65b71e8


QUERYALLFROMCOLLECTION = """
    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
    PREFIX dcterms: <http://purl.org/dc/terms/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX ore: <http://www.openarchives.org/ore/terms/>

    SELECT ?donnee ?titre
    WHERE {?fiche ore:isAggregatedBy <http://www.nakala.fr/collection/11280/c65b71e8> .
    ?fiche foaf:primaryTopic ?donnee .
    ?donnee dcterms:title ?titre .
    }
    """