# -*- coding: utf-8 -*-
"""
Created on Jul 15 15:48:25 2019

@author: Aurelia Vasile (MSH Clermont Ferrand)

"""
import csv
from generateMetaNakala import generateOneMetaNakala
from apiMethods import requestToNakala
from buildMethods import buildZipFile
import os
from constantes import WORKFOLDER, EMAIL, TOKEN, INPUTFILE, OUTPUCOPY, HANDLEPROJECT
import shutil
import pandas as pd

if EMAIL == "TO REPLACE":
    EMAIL = input ("Saisir l'email de connexion à NAKALA : ")

if TOKEN == "TO REPLACE":
    TOKEN = input ("Saisir le token NAKALA pour utiliser les API : ")

#Sauvegarde le fichier d'origine
shutil.copyfile (INPUTFILE, OUTPUCOPY)

#on ouvre le csv dans une dataFrame pour la MAJ du handle
dataFrameCSV = pd.read_csv(INPUTFILE, delimiter=",")

with open(INPUTFILE, "r") as fichiercsv:
    reader = csv.DictReader(fichiercsv, delimiter =",")

    # pour chaque ligne du fichier csv
    for y, ligne in enumerate (reader):
        # si la colonne "handle" ne contient pas un identifiant handle, générer un fichier xml
        if ligne["handle"] == "":
            generateOneMetaNakala(ligne)

            # se déplaceer dans  le répertoire paquetTestApi pour créer les zip
            os.chdir(WORKFOLDER)

            # appel de la méthode buildZipFile pour créer le zip
            buildZipFile (ligne["CoteNiveau3"])

            # revenir dans le dossier nakalaCalames pour continuer les traitements
            os.chdir("../")

            # appel de la méthode requestToNakala pur déposer les fichiers et récupérer le handle
            handleID = requestToNakala (ligne["CoteNiveau3"], EMAIL, TOKEN, HANDLEPROJECT)

            # stoque le handle dans un dataframe avec les données du fichiers csv
            dataFrameCSV.loc[y,'handle'] = handleID

        else:
            print ("déja traité "+ ligne["handle"])

#on ecrase le fichier csv source avec le dataframe contenant le handle
dataFrameCSV.to_csv(INPUTFILE, index=False)