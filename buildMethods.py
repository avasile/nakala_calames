from SPARQLWrapper import SPARQLWrapper, JSON
import  json
from zipfile import ZipFile

def requeteSparql (sparqlPoint, query):
    """
    permet de faire une requête sparql. transforme les résultats sous la forme d'un json
    :param sparqlPoint: lien url du triplestore (str)
    :param query: la reqête sparql (str)
    :return: le résultat de la requête en format json.
    """

    sparql = SPARQLWrapper(sparqlPoint)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()

    # transforme en objet json les résultats du triplestrore
    resultsJson = json.dumps(results)

    return resultsJson


def extractHandleFromNakala (jsonResultSparql):
    """
    extraction de l'identifiant handle de la réponse sparql sur le triplestore Nakala
    :param jsonResultSparql: un objet json
    :return: les identifiants handle (list)
    """
    listHandleFromNaklala = []

    chaineJson = json.loads(jsonResultSparql)

    for result in chaineJson["results"]["bindings"]:
        lienHandle = result["donnee"]["value"].split("data/")

        handleIdentfier = lienHandle[1]

        listHandleFromNaklala.append(handleIdentfier)

    return listHandleFromNaklala


def buildZipFile (fileName):
    """
    génère des fichiers zip contenant un fichier xml et un fichier jpg.
    en cas d'erreur, il génère un fichier zip avec un fichier xml et un fichier pdf.
    :param fileName: str. chaine de carractères conrrespondant au nom du fichier sans l'extention. la même que la cote
    :return:
    """
    try:

        with ZipFile(fileName+'.zip','w') as zip:
            zip.write(fileName+'.xml')
            zip.write(fileName+'.jpg')

    except FileNotFoundError:
        with ZipFile(fileName + '.zip', 'w') as zip:
            zip.write(fileName + '.xml')
            zip.write(fileName + '.pdf')

