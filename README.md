# Depôt Nakala/Calames
Scripts qui génèrent d'une part un fichier XML-ead format Calames et d'autre part, des fichiers xml en format Nakala pour chaque ressource (niveau C dans EAD) et dépose les ressources sur le serveur de Nakala.
## Description de fichers exécutables
### generateNiveauxC.py
prend en entrée un fichier csv et produit en sortie un fichier xml-ead pour Calames qui regroupe tous les niveaux C. Pour la partie eadheader et archdesc, le fichier data/headerArchdesc.xml est généré séparement (soit à la main soit avec le script createArchdesc.py).Contient les fonctions *createLevel2* et *createLevel3*
### generateMetaNakala.py
contient la fonction *generateOneMetaNakala* qui génère un fichier xml Nakala pour chaque ligne du fichier csv donnée en entrée.
### apiMethods.py
contient les méthodes *requestToNakala* qui permet de faire une requête http POST pour déposer les données sur Nakala et *deleteHandleFromNakala* qui permet de supprimer une ressource posedant un identifiant handle donnée avec la methode http DELETE.
### buidMethods.py
contient les méthodes *requeteSparql* pour réaliser une requête sparql sur le triplestore de Nakala; *extractHandleFromNakala* pour extraire uniquement l'identifiant handle de la réponse envoyé par le serveur; *buildZipFile* pour générer le fichier zip contenant le fichier xml avec les métadonnées et la resosurce décrite
### runNakala.py
appelle les méthodes définies auparavant pour lancer une chaine de traitement pour chaque ligne du csv:
* production du fichier xml de métadonnées
* construction du fichier zip (nécessie au préalable d'avoir dans le dossier en entrée les fichiers ressources (pdf ou jpg))
* l'appel http POST et dépôt du zip sur le serveur de Nakala
* stoquer l'identifiant handle dans un dataframe
* remplacer le fichier csv par le dataframe. 
### renameConvertFile.py
script qui permet de convertir des formats de fichier images et de renommer des fichiers
### testParse.py
script qui permet de tester la génération des métadonnées pour Nakala



