# -*- coding: utf-8 -*-
"""
Created on Jul 15 15:48:25 2019

@author: Aurelia Vasile (MSH Clermont Ferrand)

File to execute to test the metadata for Nakala

"""
import xml.etree.ElementTree as ET
import csv
from constantes import WORKFOLDER, INPUTFILE, CODEROLEAUTEUR
from extract.extractCsvData import extractRoleNormalPersname, extractNormaliseDates



with open(INPUTFILE, "r") as fichiercsv:
    reader = csv.DictReader(fichiercsv, delimiter =",")

    # pour chaque ligne du fichier csv
    for y, ligne in enumerate (reader):
        racine = ET.Element ("nkl:Data")
        racine.set("xmlns:nkl","http://nakala.fr/schema#")
        racine.set("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance")
        racine.set("xmlns:dcterms","http://purl.org/dc/terms/")
        racine.set("xsi:schemaLocation","http://purl.org/dc/terms/ http://dublincore.org/schemas/xmls/qdc/2008/02/11/dcterms.xsd")

        #appel des métodes pour extraire les données

        #Personnes
        pairRoleIdrefPersname3 = extractRoleNormalPersname(ligne["persnameNiveau3"], ligne["rolePersname3"],
                                                           ligne["idrefpersname3"], ligne["CoteNiveau3"],
                                                           CODEROLEAUTEUR)
        #dates
        datenorm2 = extractNormaliseDates(ligne["DateNiveau2Debut"])

        datenorm3 = extractNormaliseDates(ligne["DateNiveau3Debut"])


        ### ---------CEATION BALISES OBLIGATOIRES---------

        #--------Titre----------
        if ligne["TitreNiveau3"]:
            title = ET.SubElement(racine, "dcterms:title")
            title.text = ligne["TitreNiveau3"]
        else:
            print("Il manque le titre pour la ligne: ", ligne["CoteNiveau3"])

        #-------Creator--------
        creatorPremier = ""

        #si le champs persname existe, prendre la première personne avec le rôle auteur/070
        if ligne["persnameNiveau3"] :
            for tuple in pairRoleIdrefPersname3:
                if tuple[1] == "070":
                    creator = ET.SubElement(racine, "dcterms:creator")
                    creator.text = tuple[0]
                    creatorPremier = tuple[0]
                    break

        # sinon, prendre comme auteur le corpname du niveau 3
        elif ligne["persnameNiveau3"] == "" and ligne["corpnameNiveau3"] != "":
                creator = ET.SubElement(racine, "dcterms:creator")
                creator.text = ligne["corpnameNiveau3"]
                creatorPremier = ligne["corpnameNiveau3"]

        # sinon, prnedre comme auteur le corpname du niveau 2
        elif ligne["persnameNiveau3"] == "" and ligne["corpnameNiveau3"] == "" and ligne["corpnameNiveau2"] != "":
            creator = ET.SubElement(racine, "dcterms:creator")
            creator.text = ligne["corpnameNiveau2"]
            creatorPremier = ligne["corpnameNiveau2"]

        else:
            print("Il manque le creator pour la ligne ", ligne["CoteNiveau3"] )


        #Type Standard

        if ligne["typeDcterms"]:
            typologieStandard = ET.SubElement(racine, "dcterms:type")
            typologieStandard.set("xsi:type",'dcterms:DCMIType')
            typologieStandard.text = ligne["typeDcterms"]
        else:
            print("Il manque le type pour la ligne ", ligne["CoteNiveau3"])

        # DATE

        date = ET.SubElement(racine, "dcterms:created")

        if ligne["DateNiveau3Debut"] != "":
            date.text = datenorm3
        elif ligne["DateNiveau3Debut"] == "" and ligne["DateNiveau2Debut"] != "":
            date.text = datenorm2
        elif ligne["DateNakala"] != "":
            date.text = ligne["DateNakala"]
        elif ligne["DateNiveau3Debut"] == "" and ligne["DateNiveau2Debut"] == "" and ligne["DateNakala"] =="":
            print("Il manque la date pour la ligne ", ligne["CoteNiveau3"])


        ## BALISES COMPLEMENTAIRES


        # ----CONTRIBUTOR--------

        # si le champs persname et corpname existe en même temps Niveau3 et si le nom est differit du creatorPremier
        if ligne["persnameNiveau3"]:
            for tuple in pairRoleIdrefPersname3:
                if tuple[0] != creatorPremier and tuple[1] == "070":
                    #générer une balise autuer, si l'auteur n'existe pas déja creatorPremier
                    creator = ET.SubElement(racine, "dcterms:creator")
                    creator.text = tuple[0]
                    creatorHeader = tuple[0]

                # sinon, générer une balise cpontributor pour tout ceux qui n'ont pas l'attribut auteur
                elif  tuple[0] != creatorPremier and tuple[1] != "070":
                    contributeur = ET.SubElement(racine, "dcterms:contributor")
                    contributeur.text = tuple[0]

            ##contributeur = ET.SubElement(racine, "dcterms:contributor")
            ##contributeur.text = ligne["corpnameNiveau3"]

        if ligne["corpnameNiveau3"] != "" and ligne["corpnameNiveau3"] != creatorPremier:
            # générer une balise contributor
            contributeur = ET.SubElement(racine, "dcterms:contributor")
            contributeur.text = ligne["corpnameNiveau3"]

        #----IDENTIFIANT LOCAL-----#
        identifiant = ET.SubElement(racine, "dcterms:identifier")
        identifiant.text = ligne["CoteNiveau3"]

        # -----DESCRITPION-----#
        description = ET.SubElement(racine, "dcterms:description")
        description.text = ligne["scopecontentNiveau3"]

        #-----TYPE INTELLECTUEL DE DOCUMENT----
        if ligne["physfacetNiveau3"] == "":
            typologieIntelect = ET.SubElement(racine, "dcterms:type")
            typologieIntelect.text = ligne["physfacetNiveau2"]
        else:
            typologieIntelect = ET.SubElement(racine, "dcterms:type")
            typologieIntelect.text = ligne["physfacetNiveau3"]

        #-----EXTENT------
        extent = ET.SubElement(racine, "dcterms:extent")
        extent.text = ligne["extentNiveau3"]

        #------FORMAT NUMERIQUE------#

        formatConservation = ET.SubElement(racine,"dcterms:format")
        formatConservation.text = ligne["formatNumerique"]


        ##### ELEMENTS PAR DEFAUT #####


        #--------DROITS--------#
        if ligne["rights"]:
            detenteurDroits = ET.SubElement(racine, "dcterms:rights")
            detenteurDroits.text = ligne["rights"]
            detenteurDroits.set("xsi:type", "dcterms:URI")

        #-------PUBLISHER--------

        publisher = ET.SubElement(racine, "dcterms:publisher")
        publisher.text = "Maison des Sciences de l'Homme de Clermont-Ferrand"

        #_------LIEU DE PUBLICATION-----#

        lieuDePublication = ET.SubElement(racine, "dcterms:coverage")
        lieuDePublication.text = "Clermont-Ferrand"

        # --------LICENCE --------#
        if ligne["license"]:
            licence = ET.SubElement(racine, "dcterms:license")
            licence.text = ligne["license"]
            licence.set("xsi:type", "dcterms:URI")

        #-----COLLECTION NAKALA------#
        collection= ET.SubElement(racine,"nkl:inCollection")
        collection.text = "11280/b670021f"

        # ----ACCES RESERVE-----
        if ligne["accessEmail"] != "":
            email = ET.SubElement(racine,"nkl:accessEmail")
            email.text = ligne["accessEmail"]

        tree = ET.ElementTree(racine)
        tree.write(WORKFOLDER+"/"+ligne["CoteNiveau3"]+".xml", encoding="UTF-8")
        #if y == 10:
            #break