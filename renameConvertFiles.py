# -*- coding: utf-8 -*-
"""
Created on Jul 15 15:48:25 2019

@author: Aurelia Vasile (MSH Clermont Ferrand)

Script qui prend en entrée un fichier tiff multipages
l'eclate en autant de fichiers tiff que le nombre de pages
ajoute des 0 à une numérotation de type "Cam_E_008_18.tiff", "Cam_E_008_7.tiff", "Cam_E_008_16.tiff", etc
pour obtenir des résultats du type "Cam_E_008_018.tiff", "Cam_E_008_007.tiff", "Cam_E_008_016.tiff"

Convertit ensuite les fichiers .tiff en .jpg

"""
# librarie Python pour appliquer des commandes ImageMagik
from wand.image import Image

# librairie pour se déplacer dans les répertoires
import os
# librairie pour utiliser les expressions régulières
import re

# définir la longueur des  numéros. Ici des numéros avec 3 chiffres.
sizeNumber = "999"

# le chemin du dossier contenant LE fichier tiff de type multipage à spliter
inputPathOrig = "/Users/auvasile/Documents/TEST/"
# le chemin du dossier de conservation recevant les fichiers tiff résultant du split et renommés
pathConserv = "/Users/auvasile/Documents/Archives_campement/NumerisationConservation/CAM_E/"
# le chemin du dossier de diffusion recevant les fichiers jpg résultant de la conversion des fichiers tiff du dossier de conservation
outputPathDiffus = "/Users/auvasile/Documents/Archives_campement/NumerisationDiffusion/CAM_E/"

# se deplacer dans le repertoir avec LE fichier tiff multiplages
path = os.chdir(inputPathOrig)

# diviser en pages individuelles le fichier tiff multiplage.
os.system('convert Cam_E_008.tiff -set filename:f "%[t]_%[fx:t+1]" +adjoin "%[filename:f].tiff"')



# utiliser la méthode scandir de os pour accéder aux fichiers .tiff extraits avec ImageMagik dans le dossier inputPathOrig
with os.scandir(inputPathOrig) as it:
    for file in it:

        try :
            # utiliser la methode .name sur file pour repérer uniquement les fichiers avec l'extension donnée (ici tiff).
            if file.name.endswith(".tiff") and len(file.name)>14:

                # identifier la chaine de carractère qui correspond au dernier chiffres avant le point.
                # exemple : dans la chaine "Cam_E_008_12.tiff" identifier uniquement le 12 et le retyper en str
                objetnumero = re.search(r"[0-9]+\b", file.name)
                numero= objetnumero.group(0)
                numeroSansZeroStr = str(numero)

                # sauvegarder le dernier numéro de la chaine (ex. 12) dans une variable
                numeroAremplacer = re.compile(r"[0-9]+\b")

                # ajouter autant de 0 que la différence entre la longueur prédéfinie (ici 3) et la longueur du numéro à remplacer

                if len(sizeNumber) - len(numeroSansZeroStr) == 2:
                    fileNameAvecZero = numeroAremplacer.sub("00" + numeroSansZeroStr, file.name)

                elif len(sizeNumber) - len(numeroSansZeroStr) == 1 :
                    fileNameAvecZero = numeroAremplacer.sub("0" + numeroSansZeroStr, file.name)

                #print(fileNameAvecZero)

                # remplacer l'ancien numéro par le nouveau et le sauvegarder dans le dossier de conservation
                os.rename(inputPathOrig + file.name, pathConserv + fileNameAvecZero)


                # -------CONVERTIR EN JPG-------

                # extraire le nom du fichier sans extension. Exemple: Cam_E_008_12
                fileNameAvecZeroSansExtension = fileNameAvecZero[:-5]

                # ouvrir les fichiers tiff stockés dans le dossier de conservation, les convertir en jpg et les sauvegarder dans le répertoire de diffusion
                with Image(filename = pathConserv + fileNameAvecZero) as img:
                    img.format = 'jpeg'
                    img.save(filename = outputPathDiffus + fileNameAvecZeroSansExtension + ".jpg")

        except NameError:
            print("les fichiers sont déja renommés ")




