from constantes import EMAIL, TOKEN, SPARQLPOINT, QUERYALLFROMCOLLECTION
from buildMethods import extractHandleFromNakala, requeteSparql
from apiMethods import deleteHandleFromNakala


# appel de la méthode requeteSparql qui récupère un json avec les données de Nakalapour une collection donnée
dataForDelete = requeteSparql(SPARQLPOINT, QUERYALLFROMCOLLECTION)

# appel de la méthode extractHandleFromNakala pour extraire uniquement les identifiants Handle (dans une liste)
listhandlefromNakala= extractHandleFromNakala(dataForDelete)


if EMAIL == "TO REPLACE":
    EMAIL = input ("Saisir l'email de connexion à NAKALA : ")

if TOKEN == "TO REPLACE":
    TOKEN = input ("Saisir le token NAKALA pour utiliser les API : ")

# pour chaque handle de la liste, appel de la méthode deleteHandleFromNakala qui fait un appel http avec la methode DELETE
for handle in listhandlefromNakala:

    messageDelete = deleteHandleFromNakala(EMAIL, TOKEN, handle)
    print(messageDelete)