import requests

from constantes import WORKFOLDER
import json

def requestToNakala (fileName, email, token, project):
    """
    appel http à Nakala avec la méthode POST pour déposer les ressources
    :param fileName: le nom du fichier
    :type fileName : str
    :param email: email de connexion
    :param token: la clé de connexion
    :return: identifiant handle
    :type str
    """

    url = "https://www.nakala.fr/nakala/api/v1/data"
    params = {"email": email, "key": token, "project": project}
    headers = {
        'Content-Type': "application/octet-stream",
        'accept-encoding': "gzip, deflate",
    }

    response = requests.request("POST", url, data=open(WORKFOLDER+"/"+fileName + ".zip", "rb"), headers=headers, params=params)
    # lit un objet json
    json_data = json.loads(response.text)

    if json_data["message"] == "Accès non autorisé":
        pass
    else:
        handleID = json_data["handleId"]
        return handleID



def deleteHandleFromNakala (email, token, handle):
    """
    fonction qui supprime une donnée sur Nakala avec son identifiant handle donnée
     :param email: email de connexion
    :param token: la clé de connexion
    :param handle: l'identifiant handle (str)
    :return: réponse http.
    :type: json
    """
    url = "https://www.nakala.fr/nakala/api/v1/data/"
    params = {"email": email, "key": token}
    headers = {
        'Content-Type': "application/octet-stream"
    }
    response = requests.request("DELETE", url+handle, headers=headers, params=params)
    reponse = response.text
    return reponse